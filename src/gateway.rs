/*
 * Copyright (c) 2021, 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Find the current gateway address.

use std::process::Stdio;
use std::time::Duration;

use serde::Deserialize;
use tokio::io::AsyncReadExt;
use tokio::process::Command;
use tokio::sync::mpsc::Sender;
use tokio::time;

use crate::defs::{Config, Error, MainMsg, UiPrio};

/// The representation of a single IPv4 route.
#[derive(Debug, Deserialize)]
struct Route {
    /// The destination network.
    dst: Option<String>,
    /// The IPv4 gateway address.
    gateway: Option<String>,
}

/// Run `ip route list` in a loop until we find a gateway address.
///
/// # Errors
///
/// [`Error::ChildPipe`] on trouble with the child process.
#[inline]
async fn find_gateway_loop<C: Config>(cfg: &C) -> Result<String, Error> {
    loop {
        cfg.diag_("find_gateway: running 'ip route list'");
        match Command::new("ip")
            .args(["-4", "-j", "route", "list"])
            .envs(cfg.utf8_vars())
            .stdout(Stdio::piped())
            .kill_on_drop(true)
            .spawn()
        {
            Err(err) => {
                cfg.diag(|| format!("find_gateway: could not spawn 'ip route list': {}", err));
            }
            Ok(child) => {
                let mut raw = vec![];
                match child
                    .stdout
                    .ok_or(Error::ChildPipe)?
                    .read_to_end(&mut raw)
                    .await
                {
                    Err(err) => cfg.diag(|| {
                        format!(
                            "find_gateway: could not read the output of 'ip route list': {}",
                            err
                        )
                    }),
                    Ok(_) => match serde_json::from_slice::<Vec<Route>>(&raw) {
                        Err(err) => cfg.diag(|| {
                            format!("find_gateway: 'ip route list': invalid JSON: {}", err)
                        }),
                        Ok(data) => {
                            match data.into_iter().find(|route| {
                                route.dst.as_ref().map_or(false, |dst| dst == "default")
                            }) {
                            Some(route) => match route.gateway {
                                Some(gateway) => {
                                    cfg.diag(|| format!("find_gateway: got {}", gateway));
                                    return Ok(gateway);
                                }
                                None => cfg.diag_("find_gateway: there is a default route, but no gateway address"),
                            }
                            None => cfg.diag_("find_gateway: no default IPv4 gateway address, waiting")
                        }
                        }
                    },
                }
            }
        };
        cfg.diag_("find_gateway: waiting for a while");
        time::sleep(Duration::new(1, 0)).await;
    }
}

/// Run a command to obtain the current IPv4 gateway address.
///
/// # Errors
///
/// [`Error::ChildPipe`] on trouble with the child process.
#[inline]
pub async fn find_gateway<C: Config>(cfg: C, tx: Sender<MainMsg>) -> Result<(), Error> {
    cfg.diag_("find_gateway: looking for a gateway");
    let gateway = find_gateway_loop(&cfg).await?;
    cfg.diag(|| format!("find_gateway: out of the loop: {:?}", gateway));
    if let Err(err) = tx.send(MainMsg::Gateway(gateway)).await {
        cfg.ui_msg(
            UiPrio::Error,
            &format!("find_gateway: could not tell the main thread: {}", err),
        );
    }
    Ok(())
}
