/*
 * Copyright (c) 2021, 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Handle SIGINT and SIGTERM by sending a "go away" message.

use futures::stream::StreamExt;
use signal_hook::consts::signal::{SIGINT, SIGTERM};
use signal_hook_tokio::{Handle, Signals};
use tokio::sync::mpsc::Sender;

use crate::defs::{Config, Error, MainMsg, UiPrio};

/// Post a [`crate::defs::MainMsg::StopRequested`] message on SIGINT or SIGTERM.
async fn handle_signals<C: 'static + Config>(
    cfg: C,
    sigs: Signals,
    tx: Sender<MainMsg>,
) -> Result<(), Error> {
    let mut signals = sigs.fuse();
    while let Some(sig) = signals.next().await {
        match sig {
            SIGINT | SIGTERM => {
                cfg.diag(|| format!("Got a 'go away' signal {}, going away", sig));
                if let Err(err) = tx.send(MainMsg::StopRequested).await {
                    cfg.ui_msg(
                        UiPrio::Error,
                        &format!("signals: could not tell the main thread to stop: {}", err),
                    );
                }
                break;
            }

            other => super::die(&format!("Got unexpected signal {}", other)),
        }
    }
    Ok(())
}

/// Set up the signal handler for SIGINT and SIGTERM.
///
/// # Errors
///
/// [`Error::SignalInit`] if the `signal_hook_tokio` setup fails.
#[inline]
pub fn setup_signal_handlers<C: 'static + Config>(
    cfg: &C,
    tx: Sender<MainMsg>,
) -> Result<(Handle, tokio::task::JoinHandle<Result<(), Error>>), Error> {
    cfg.diag_("Setting up the signal handlers");
    let sigs = Signals::new([SIGINT, SIGTERM]).map_err(Error::SignalInit)?;
    let handle = sigs.handle();
    Ok((
        handle,
        tokio::spawn(handle_signals((*cfg).clone(), sigs, tx)),
    ))
}
