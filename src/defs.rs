/*
 * Copyright (c) 2021, 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Common definitions for the "ping the gateway" tool.

use std::collections::HashMap;
use std::io::Error as IoError;

use config_diag::ConfigDiag;
use regex::Error as RegexError;
use thiserror::Error;
use tokio::sync::mpsc::error::SendError;

/// An error that occurred while setting up the signal handlers.
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    /// Could not obtain the child process's pid.
    #[error("Could not obtain a child process's ID")]
    ChildGetId,

    /// Could not get the child process's stdout stream.
    #[error("Could not set up a pipe for a child process")]
    ChildPipe,

    /// Could not send a message via an internal channel.
    #[error("Could not send a message via an internal channel")]
    MsgSend(#[source] SendError<MainMsg>),

    /// Could not compile a regular expression.
    #[error("Internal error: could not compile a regular expression")]
    Regex(#[source] RegexError),

    /// Could not set up the signals structure.
    #[error("Could not initialize the signals structure")]
    SignalInit(#[source] IoError),
}

/// How urgent a UI message can be.
#[derive(Debug)]
#[non_exhaustive]
pub enum UiPrio {
    /// An informational message.
    Info,
    /// An error message.
    Error,
}

/// Runtime configuration for the "ping the gateway" tool.
pub trait Config: Clone + Send + Sync + ConfigDiag {
    /// Display a message using the currently selected user interface.
    fn ui_msg(&self, prio: UiPrio, msg: &str);

    /// Get the variables needed to force a UTF-8-capable environment.
    fn utf8_vars(&self) -> HashMap<String, String>;
}

/// Messages posted to the main queue.
#[derive(Debug)]
#[non_exhaustive]
pub enum MainMsg {
    /// There is currently no gateway to ping.
    NoGW,
    /// Found an IPv4 default gateway address.
    Gateway(String),
    /// Got a ping reply!
    PingReply,
    /// Somebody told us to go away.
    StopRequested,
}
