/*
 * Copyright (c) 2021, 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Implement the main pinggw loop, spawning all the necessary tasks:
//! signals handling, wait for an IPv4 default gateway address,
//! run a ping process against it, wait for a timeout.

use std::time::Duration;

use tokio::sync::mpsc::{self, Sender};
use tokio::task::JoinHandle;
use tokio::time;

use crate::defs::{Config, Error, MainMsg, UiPrio};
use crate::gateway;
use crate::ping;
use crate::signals;

/// The current state: looking for a gateway, pinging a gateway, etc.
#[derive(Debug)]
enum MainState {
    /// No route or gateway information yet, just starting up.
    Init,
    /// Looking for a gateway.
    /// The argument is the [`crate::gateway::find_gateway_loop`] task.
    FindGateway(JoinHandle<Result<(), Error>>),
    /// Pinging the active gateway.
    /// The arguments are the  [`crate::ping::ping`] and the [`post_timeout`] tasks.
    Ping(JoinHandle<Result<(), Error>>, JoinHandle<Result<(), Error>>),
    /// Cleaning up before exiting.
    Done,
}

/// Wait for a spawned task to finish, report errors.
async fn stop_task<C: Config>(
    cfg: &C,
    tag: &str,
    task: JoinHandle<Result<(), Error>>,
    cancel: bool,
) {
    if cancel {
        cfg.diag(|| format!("main: cancelling the {} task", tag));
        task.abort();
    }
    cfg.diag(|| format!("main: waiting for the {} task to end", tag));
    match task.await {
        Ok(Ok(())) => cfg.diag(|| format!("main: {} task done", tag)),
        Ok(Err(err)) => cfg.ui_msg(
            UiPrio::Error,
            &format!("The {} task reported an error: {}", tag, err),
        ),
        Err(err) => {
            if err.is_cancelled() {
                cfg.diag(|| format!("main: {} task cancelled", tag));
            } else {
                cfg.ui_msg(
                    UiPrio::Error,
                    &format!("Could not shut the {} task down: {}", tag, err),
                );
            }
        }
    }
}

/// Wait for a while, post a "timeout" message to the main event queue.
/// This task is supposed to be cancelled whenever a ping reply is received.
async fn post_timeout<C: Config>(cfg: C, tx: Sender<MainMsg>) -> Result<(), Error> {
    time::sleep(Duration::new(5, 0)).await;
    cfg.ui_msg(UiPrio::Error, "main: ping timeout!");
    cfg.diag_("main: posting a 'no gateway' message due to a timeout");
    if let Err(err) = tx.send(MainMsg::NoGW).await {
        cfg.ui_msg(
            UiPrio::Error,
            &format!(
                "main: could not post a 'no gateway' timeout message: {}",
                err
            ),
        );
    }
    Ok(())
}

/// Handle a message posted by a task to the main queue.
#[allow(clippy::wildcard_enum_match_arm)]
async fn handle<C: 'static + Config>(
    cfg: &C,
    state: MainState,
    msg: MainMsg,
    tx: &Sender<MainMsg>,
) -> MainState {
    let msg_fmt = format!("got a message {:?} in state {:?}", msg, state);
    let bad_msg = || super::die(&msg_fmt);
    cfg.diag(|| format!("main: {}", msg_fmt));
    match state {
        MainState::Init => match msg {
            MainMsg::NoGW => {
                cfg.ui_msg(UiPrio::Info, "Let's find us an IPv4 gateway first");
                cfg.diag_("main: spawning a 'find a gateway' task");
                MainState::FindGateway(tokio::spawn(gateway::find_gateway(cfg.clone(), tx.clone())))
            }
            _ => bad_msg(),
        },
        MainState::FindGateway(task) => match msg {
            MainMsg::NoGW => {
                cfg.diag_("main: ignoring a delayed 'no gateway' message");
                MainState::FindGateway(task)
            }
            MainMsg::PingReply => {
                cfg.diag_("main: ignoring a delayed ping reply");
                MainState::FindGateway(task)
            }
            MainMsg::StopRequested => {
                cfg.diag_("main: somebody wants us to go away");
                stop_task(cfg, "gateway", task, true).await;
                MainState::Done
            }
            MainMsg::Gateway(gateway) => {
                cfg.ui_msg(UiPrio::Info, &format!("Got a gateway: {}", gateway));
                stop_task(cfg, "gateway", task, false).await;
                cfg.diag_("main: spawning a ping task");
                MainState::Ping(
                    tokio::spawn(ping::ping(cfg.clone(), tx.clone(), gateway)),
                    tokio::spawn(post_timeout(cfg.clone(), tx.clone())),
                )
            }
        },
        MainState::Ping(ping_task, timeout_task) => match msg {
            MainMsg::StopRequested => {
                cfg.diag_("main: somebody wants us to go away");
                stop_task(cfg, "ping", ping_task, true).await;
                stop_task(cfg, "timeout", timeout_task, true).await;
                MainState::Done
            }
            MainMsg::NoGW => {
                cfg.diag_("main: lost our gateway for some reason");
                stop_task(cfg, "ping", ping_task, true).await;
                stop_task(cfg, "timeout", timeout_task, true).await;
                cfg.diag_("main: waiting for a while");
                time::sleep(Duration::new(1, 0)).await;
                cfg.diag_("main: back to looking for a gateway");
                MainState::FindGateway(tokio::spawn(gateway::find_gateway(cfg.clone(), tx.clone())))
            }
            MainMsg::PingReply => {
                stop_task(cfg, "timeout", timeout_task, true).await;
                MainState::Ping(
                    ping_task,
                    tokio::spawn(post_timeout(cfg.clone(), tx.clone())),
                )
            }
            _ => bad_msg(),
        },
        MainState::Done => bad_msg(),
    }
}

/// The main pinggw loop: wait for a gateway, run a ping process, check for timeout.
///
/// # Errors
///
/// Propagates errors returned by [`Sender::send`].
#[inline]
pub async fn run<C: 'static + Config>(cfg: &C) -> Result<(), Error> {
    cfg.diag_("main: starting up");

    let (tx, mut rx) = mpsc::channel(16);
    let (signals_handle, signals_task) = signals::setup_signal_handlers(cfg, tx.clone())?;
    {
        tx.send(MainMsg::NoGW).await.map_err(Error::MsgSend)?;
    }
    let mut state = MainState::Init;
    while let Some(msg) = rx.recv().await {
        state = handle(cfg, state, msg, &tx).await;
        if let MainState::Done = state {
            break;
        }
    }

    cfg.diag_("main: shutting down the receiver");
    rx.close();

    cfg.diag_("main: shutting down the signals task");
    signals_handle.close();
    stop_task(cfg, "signals", signals_task, false).await;

    match state {
        MainState::Init => cfg.diag_("main: nothing to clean up that early"),
        MainState::FindGateway(task) => {
            stop_task(cfg, "gateway", task, true).await;
        }
        MainState::Ping(ping_task, timeout_task) => {
            stop_task(cfg, "ping", ping_task, true).await;
            stop_task(cfg, "timeout", timeout_task, true).await;
        }
        MainState::Done => cfg.diag_("main: nothing to stop any more"),
    }

    cfg.diag_("main: run: returning");
    Ok(())
}
