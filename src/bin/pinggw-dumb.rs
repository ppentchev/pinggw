#![deny(missing_docs)]
/*
 * Copyright (c) 2021, 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Ping the gateway: a dumb terminal command-line interface.

// Activate most of the clippy::restriction lints...
#![warn(clippy::indexing_slicing)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::print_stderr)]
#![warn(clippy::print_stdout)]
#![warn(clippy::shadow_reuse)]
#![warn(clippy::shadow_unrelated)]
#![warn(clippy::unwrap_used)]
// ...except for these ones.
#![allow(clippy::implicit_return)]
// Activate some of the clippy::restriction lints...
#![warn(clippy::semicolon_if_nothing_returned)]

use std::collections::HashMap;
use std::process;

use anyhow::{Context, Result};
use clap::Parser;
use config_diag::ConfigDiag;
use utf8_locale::Utf8Detect;

use pinggw::defs::{Config as DefsConfig, UiPrio};

/// Runtime configuration for the pinggw command-line tool.
#[derive(Debug, Clone)]
struct Config {
    /// Environment variables to set for UTF-8 program output.
    utf8_vars: HashMap<String, String>,
    /// Display diagnostic output.
    verbose: bool,
}

impl ConfigDiag for Config {
    fn diag_is_verbose(&self) -> bool {
        self.verbose
    }
}

impl DefsConfig for Config {
    #[allow(clippy::print_stderr)]
    #[allow(clippy::print_stdout)]
    #[allow(clippy::use_debug)]
    fn ui_msg(&self, prio: UiPrio, msg: &str) {
        match prio {
            UiPrio::Info => println!("{}", msg),
            UiPrio::Error => eprintln!("{}", msg),
            _ => {
                eprintln!("Unexpected message priority: {:?} for {:?}", prio, msg);
                process::exit(1);
            }
        }
    }

    fn utf8_vars(&self) -> HashMap<String, String> {
        self.utf8_vars.clone()
    }
}

/// Parse the command-line arguments.
#[derive(Debug, Parser)]
struct Cli {
    /// Verbose operation; display diagnostic output.
    #[clap(short, long)]
    verbose: bool,
}

/// Parse the command-line arguments.
fn parse_args() -> Result<Config> {
    let args = Cli::parse();
    Ok(Config {
        utf8_vars: Utf8Detect::new()
            .detect()
            .context("Could not determine a UTF-8 locale to use")?
            .env_vars,
        verbose: args.verbose,
    })
}

#[tokio::main]
async fn main() -> Result<()> {
    let cfg = parse_args()?;
    pinggw::run(&cfg).await.context("Something went wrong")
}
