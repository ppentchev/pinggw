/*
 * Copyright (c) 2021, 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Ping the default gateway, send responses to the main thread.

use std::process::Stdio;

use once_cell::sync::Lazy;
use regex::{Error as RegexError, Regex};
use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::process::Command;
use tokio::sync::mpsc::Sender;

use crate::defs::{Config, Error, MainMsg, UiPrio};

/// Recognize a "ping reply" line in the ping(8) output.
const RE_PING_LINE: &str = r"(?x)
    ^
    \d+ \s+ bytes \s+ from \s+
    ";

/// Start a ping at the default gateway, send responses to the main thread.
///
/// # Errors
///
/// [`Error::Regex`] if the internal ping output regular expression fails to compile.
/// [`Error::ChildGetId`], [`Error::ChildPipe`] on trouble with the child process.
#[inline]
pub async fn ping<C: Config>(cfg: C, tx: Sender<MainMsg>, gateway: String) -> Result<(), Error> {
    /// Match a line of `ping` output: address, round-trip time, etc.
    static RE_LINE: Lazy<Result<Regex, RegexError>> = Lazy::new(|| Regex::new(RE_PING_LINE));

    cfg.diag(|| format!("ping: about to start a ping for {}", gateway));
    let child = match Command::new("stdbuf")
        .args(["-oL", "--", "ping", "-4", "-n", "--"])
        .arg(&gateway)
        .envs(cfg.utf8_vars())
        .stdout(Stdio::piped())
        .kill_on_drop(true)
        .spawn()
    {
        Ok(child) => child,
        Err(err) => {
            cfg.ui_msg(
                UiPrio::Error,
                &format!("ping: could not start a ping process: {}", err),
            );
            if let Err(tx_err) = tx.send(MainMsg::NoGW).await {
                cfg.ui_msg(
                    UiPrio::Error,
                    &format!(
                        "ping: could not send a 'no gateway' message to the main thread: {}",
                        tx_err
                    ),
                );
            }
            return Ok(());
        }
    };
    let child_pid = child.id().ok_or(Error::ChildGetId)?;
    cfg.diag(|| format!("ping: spawned process {}", child_pid));
    let mut stdout = BufReader::new(child.stdout.ok_or(Error::ChildPipe)?);
    let re_line = RE_LINE
        .as_ref()
        .map_err(|err| Error::Regex((*err).clone()))?;
    loop {
        let mut line = String::new();
        match stdout.read_line(&mut line).await {
            Ok(0) => {
                cfg.diag_("ping: eeeep, the ping process died!");
                break;
            }
            Ok(_) => {
                cfg.ui_msg(UiPrio::Info, line.trim_end());
                if re_line.is_match(&line) {
                    cfg.diag_("ping: notifying the main thread");
                    if let Err(err) = tx.send(MainMsg::PingReply).await {
                        cfg.ui_msg(
                            UiPrio::Error,
                            &format!("ping: could not send a message to the main thread: {}", err),
                        );
                        return Ok(());
                    }
                }
            }
            Err(err) => {
                cfg.diag(|| {
                    format!(
                        "ping: eeeep, something went wrong with the ping process: {}",
                        err
                    )
                });
                break;
            }
        }
    }

    /* Something went wrong, tell the main thread so. */
    if let Err(err) = tx.send(MainMsg::NoGW).await {
        cfg.ui_msg(
            UiPrio::Error,
            &format!(
                "ping: could not send a 'no gateway' message to the main thread: {}",
                err
            ),
        );
    }
    Ok(())
}
