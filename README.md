# pinggw - constantly ping a gateway address

This is a small program meant to help with keeping a network connection
active by constantly sending pings. It may also be useful as a quick
visual aid for figuring out when something has gone wrong.
